1. Crear un branch a partir del master.

2. Crear un api que consulte el servicio expuesto en la siguiente url:

    https://api.exchangeratesapi.io/latest

    - Dicho servicio trae los tipos de cambio de acuerdo a una moneda base, por default es EUROS
    - Se le puede solicitar el tipo de cambio de una moneda en especifico, pasandole como request param el tipo de moneda (ejemplo MXN)
    
    
3. El servicio debera recibir un parametro que indique la moneda de la cual se desea solicitar el tipo de cambio (este parametro no puede ser vacio).

4. El servicio debera recibir un parametro donde se le indique si queremos filtrar por una cantidad especifica el tipo de cambio (ejemplo: monedas con tipos de cambio mayores a 10):
   
    - Si este parametro va vacio se deberan traer todo el listado de valores sin filtrar.
    - 
5. Probar el servicio en tu maquina.

6. Compilar la arquitectura (setear la variable CONFIG_SERVICE_PASSWORD).

7. Integrar el servicio realizado en la arquitectura para que sea consumible a travez del gateway.

8. Subir sus cambios a su rama correspondiente.


Suerte!!!